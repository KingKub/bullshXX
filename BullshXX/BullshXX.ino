#include <SoftwareSerial.h>
#include <MP3Player_KT403A.h>
 

SoftwareSerial mp3(2, 3);

int TASTER = 4;
boolean warGedrueckt = false;
boolean makeAPAHappy = false;
unsigned long gedruecktAm=0;

int FILECOUNT = 109;

void setup() {
    mp3.begin(9600);
    Serial.begin(9600); 
    delay(100);
    
    SelectPlayerDevice(0x02);      
    SetVolume(0x1E);               

    randomSeed(analogRead(0));
    pinMode(TASTER, INPUT_PULLUP);
    pinMode(LED_BUILTIN, OUTPUT);
    
    Serial.println("Init Finished!");
}

void loop() {
  boolean istGedrueckt = !digitalRead(TASTER);
  Serial.print("gedrueckt: ");
  Serial.print (istGedrueckt);
  Serial.print (" happy: ");
  Serial.println (makeAPAHappy);
  if (!istGedrueckt && warGedrueckt) {
    int randNumber = random(random(1, FILECOUNT + 1));
    if (makeAPAHappy){
        SpecifyfolderPlay(1, 1);
    } else { 
        SpecifyMusicPlay(randNumber);
    }
    
    Serial.print ("playing: ");
    Serial.println (randNumber);

    if (millis() - gedruecktAm > 5000) {
       makeAPAHappy = true;
    } else {
      makeAPAHappy = false;
    }
  }
  if (istGedrueckt && !warGedrueckt) {
    gedruecktAm = millis();
  }
  if (makeAPAHappy) {
    digitalWrite(LED_BUILTIN, HIGH);
  }
  else {
    digitalWrite(LED_BUILTIN, LOW);
  }
  warGedrueckt = istGedrueckt;
    delay (50);
}
